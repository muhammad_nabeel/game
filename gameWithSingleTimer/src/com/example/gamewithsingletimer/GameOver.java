package com.example.gamewithsingletimer;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
public class GameOver extends Activity{
	Button mainpage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gameover);
		mainpage = (Button)findViewById(R.id.mainpage);
//		try{
//	        Thread.sleep(5000);
//	    }catch (InterruptedException e) {
//	        e.printStackTrace();
//	    }
		mainpage.setOnClickListener( new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent( GameOver.this , GameMenu.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				finish();
			}
		});
	}
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if(keyCode == KeyEvent.KEYCODE_BACK){   
		Log.d("CDA", "onKeyDown Called");
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }
	@Override
    public void onBackPressed() {
        Log.d("CDA", "onBackPressed Called");
        Intent setIntent = new Intent(GameOver.this , GameMenu.class);
        setIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(setIntent); 
        finish();
        return;
    }
}
