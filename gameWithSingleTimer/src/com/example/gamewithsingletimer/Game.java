package com.example.gamewithsingletimer;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Game extends Activity {
	ImageView dogA;
	ImageView dogB;
	Bitmap mBitmap, mBitmap2;
	Bitmap resizedbitmap, resizedbitmap2;
	OnTouchListener progresTouch;
	ProgressBar progresBar, progresBar2;
	ArrayList<Button> ab;
	ArrayList<Button> active;
	ArrayList<Button> inActive;
	ArrayList<Character> cats;
	TextView tv, tv2;
	LinearLayout layout;
	int startLoc;
	int startLoc3;
	boolean upDown;
	Timer tim;
	int score;
	int turns;
	int power;
	int sec;
	int consectiveThree;
	int tunnelCounter;
	int imageHeight, imageHeight2;
	int imageWidth, imageWidth2;
	boolean gameOver;
	boolean flag2;
	// Timer [] tim2;
	int currentX, currentY;
	int finalX, finalY;
	int screenHeight = 301;
	int screenWidth = 301;
	OnTouchListener progres2Touch = new OnTouchListener() {
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				// TOUCH STARTED
			case MotionEvent.ACTION_UP:
				if (power > 0 && power < 3) {
					power -= 1;
					progresBar2.setProgress(power);
					int width = imageWidth + 60;
					int height = imageHeight + 35;
					imageHeight2 = imageHeight + 35;
					imageWidth2 = imageWidth + 60;
					resizedbitmap = Bitmap.createScaledBitmap(mBitmap, width,
							height, true);
					resizedbitmap2 = Bitmap.createScaledBitmap(mBitmap2, width,
							height, true);
					runTimmer();
				} else if (power == 3) {
					power = 0;
					progresBar2.setProgress(power);
					int width = imageWidth + 100;
					int height = imageHeight + 50;
					imageHeight2 = imageHeight + 50;
					imageWidth2 = imageWidth + 100;
					resizedbitmap = Bitmap.createScaledBitmap(mBitmap, width,
							height, true);
					resizedbitmap2 = Bitmap.createScaledBitmap(mBitmap2, width,
							height, true);
					runTimmer();
				}
				return true;
			}
			return false;
		}
	};

	public enum CatSelection {
		Fat("15"), MediumFat("13"), PussyCat("12"), Slim("8"), Normal("10");
		private String catName;

		private CatSelection(String name) {
			catName = name;
		}

		public String getCatSpeed() {
			return catName;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.gamelayout);
		ab = new ArrayList<Button>();
		active = new ArrayList<Button>();
		inActive = new ArrayList<Button>();
		cats = new ArrayList<Character>();
		dogA = (ImageView) findViewById(R.id.dogA);
		dogB = (ImageView) findViewById(R.id.dogB);
		progresBar = (ProgressBar) findViewById(R.id.adprogress_progressBar);
		progresBar.setMax(10);
		progresBar.setProgress(0);
		progresBar2 = (ProgressBar) findViewById(R.id.adprogress_progressBar2);
		progresBar2.setMax(3);
		progresBar2.setProgress(0);
		dogB.setVisibility(View.INVISIBLE);
		Button but = (Button) findViewById(R.id.Fat);
		ab.add(but);
		but = (Button) findViewById(R.id.MediumFat);
		ab.add(but);
		but = (Button) findViewById(R.id.PussyCat);
		ab.add(but);
		but = (Button) findViewById(R.id.Slim);
		ab.add(but);
		but = (Button) findViewById(R.id.Normal);
		ab.add(but);
		tv = (TextView) findViewById(R.id.tvDisp);
		turns = 12;
		gameOver = false;
		power = 0;
		tunnelCounter = 10;
		consectiveThree = 0;
		flag2 = false;
		WindowManager w = getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		if (Build.VERSION.SDK_INT == 1) {
			screenWidth = metrics.widthPixels;
			screenHeight = metrics.heightPixels;
		}
		try {
			// used when 17 > SDK_INT >= 14; includes window decorations
			// (statusbar bar/menu bar)
			if (Build.VERSION.SDK_INT > 17 && Build.VERSION.SDK_INT <= 14) {
				screenWidth = (Integer) Display.class.getMethod("getRawWidth")
						.invoke(d);
				screenHeight = (Integer) Display.class
						.getMethod("getRawHeight").invoke(d);
			}
		} catch (Exception ignored) {
		}
		try {
			// used when SDK_INT >= 17; includes window decorations (statusbar
			// bar/menu bar)
			if (Build.VERSION.SDK_INT >= 17) {
				Point realSize = new Point();
				Display.class.getMethod("getRealSize", Point.class).invoke(d,
						realSize);
				screenWidth = realSize.x;
				screenHeight = realSize.y;
			}
		} catch (Exception ignored) {
		}
		// Display display = getWindowManager().getDefaultDisplay();
		// screenWidth = display.getWidth();
		// screenHeight = display.getHeight();
		startLoc = screenHeight - imageHeight;// getTextSize();
		startLoc3 = screenHeight;
		final RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(0, screenHeight, 0, 0);
		dogA.setLayoutParams(layoutParams);
		tim = new Timer();
		upDown = true;
		score = 0;
		BitmapFactory.Options dimensions = new BitmapFactory.Options();
		dimensions.inJustDecodeBounds = true;
		mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dog,
				dimensions);
		mBitmap2 = BitmapFactory.decodeResource(getResources(),
				R.drawable.dog_down, dimensions);
		imageHeight = dimensions.outHeight;
		imageWidth = dimensions.outWidth;
		imageHeight2 = dimensions.outHeight;
		imageWidth2 = dimensions.outWidth;
		mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dog);
		mBitmap2 = BitmapFactory.decodeResource(getResources(),
				R.drawable.dog_down);
		// resizedbitmap = Bitmap.createScaledBitmap(mBitmap, imageWidth,
		// imageHeight, true);
		// resizedbitmap2 = Bitmap.createScaledBitmap(mBitmap2, imageWidth,
		// imageHeight, true);
		resizedbitmap = mBitmap;
		resizedbitmap2 = mBitmap2;
		Log.e("Width", "" + imageWidth);
		Log.e("Hight", "" + imageHeight);
		progresTouch = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// TOUCH STARTED
				case MotionEvent.ACTION_UP:
					power += 1;
					progresBar.setProgress(0);
					progresBar2.incrementProgressBy(1);
					return true;
				}
				return false;
			}
		};

		final OnTouchListener progresUnableTouch = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// TOUCH STARTED
				case MotionEvent.ACTION_UP:
					return true;
				}
				return false;
			}
		};
		tim.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				runOnUiThread(new Runnable() {
					public void run() {
						if (upDown) {
							if (startLoc < 0) {
								upDown = false;
								dogA.setImageResource(R.drawable.dog_down);
								dogA.setImageBitmap(resizedbitmap2);

							} else {
								startLoc -= 1;
								layoutParams.setMargins(0, startLoc, 0, 0);
								dogA.setLayoutParams(layoutParams);
							}
						} else if (!upDown) {
							if (startLoc > screenHeight - imageHeight2) {
								upDown = true;
								dogA.setImageResource(R.drawable.dog);
								dogA.setImageBitmap(resizedbitmap);
							} else {
								startLoc += 1;
								layoutParams.setMargins(0, startLoc, 0, 0);
								dogA.setLayoutParams(layoutParams);
							}
						}
					}
				});
			}
		}, 0, 5);
		Random r = new Random();
		Character cB;
		int j = 0;
		for (Button bt : ab) {
			int location = (r.nextInt(screenWidth) - 100) + imageWidth;
			int speed = Integer.parseInt(CatSelection.valueOf(
					(String) bt.getTag()).getCatSpeed());
			Log.e("CatSelection",
					""
							+ CatSelection.valueOf((String) bt.getTag())
									.getCatSpeed());
			cB = new Character(bt, startLoc3, location, speed);// Integer.parseInt((String)CatSelection.valueOf((String)bt.getTag()));
			cats.add(cB);
			cB.createCharacter();
			Log.d("create ", "" + j++);
		}
		progresBar.setOnTouchListener(progresUnableTouch);
		progresBar2.setOnTouchListener(progres2Touch);
	}

	public void destroyCharacters() {
		for (Character obj : cats) {
			Log.e("Destroy: ", "" + obj.b.getId());
			obj.tim2.cancel();
			obj.tim2.purge();
		}
		cats.clear();
		inActive.clear();
		ab.clear();
		tim.cancel();
		if (!gameOver) {
			gameOver = true;
			Intent intent = new Intent(Game.this, GameOver.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	public void goToMainPage() {

		for (Character obj : cats) {
			Log.e("Destroy: ", "" + obj.b.getId());
			obj.tim2.cancel();
			obj.tim2.purge();
		}
		cats.clear();
		inActive.clear();
		ab.clear();
		tim.cancel();
		if (!gameOver) {
			gameOver = true;
			Intent intent = new Intent(Game.this, GoToFirstPage.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	void runTimmer() {
		final Timer T = new Timer();
		sec = 0;
		T.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				sec += 1;
				if (sec == 5) {
					flag2 = false;
					// if (upDown) {
					// Bitmap bmp = BitmapFactory.decodeResource(
					// getResources(), R.drawable.dog);
					int width = imageWidth;
					int height = imageHeight;
					imageHeight2 = imageHeight;
					imageWidth2 = imageWidth;
					resizedbitmap = Bitmap.createScaledBitmap(mBitmap, width,
							height, true);
					// dogA.setImageBitmap(resizedbitmap);
					// } else {
					// Bitmap bmp = BitmapFactory.decodeResource(
					// getResources(), R.drawable.dog_down);
					// int width = imageWidth;
					// int height = imageHeight;
					// imageHeight2 = imageHeight;
					// imageWidth2 = imageWidth;
					resizedbitmap2 = Bitmap.createScaledBitmap(mBitmap2, width,
							height, true);
					// dogA.setImageBitmap(resizedbitmap);
					// }
					//
					// // dogA.setMaxHeight( imageHeight + 40 );
					// // dogA.setMaxWidth(imageWidth + 25 );
					// // power += 1;
					tunnelCounter = 10;
					Log.e("Power", " " + sec);
					T.cancel();
				}
			}
		}, 0, 1000);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return super.onTouchEvent(event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d("CDA", "onKeyDown Called");
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onBackPressed() {
		Log.d("CDA", "onBackPressed Called");
		goToMainPage();
		return;
	}

	class Character {
		Button b;
		Timer tim2;
		int startLoc2;
		int dis;
		int speed;
		int finalX;
		int finalY;
		int baseX;
		int baseY;
		int dx, dy;
		Random r;
		int flag;
		int currentLoc;
		OnTouchListener butTouch = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// TOUCH STARTED
					baseX = (int) event.getX();
					baseY = (int) event.getY();
					return true;
				case MotionEvent.ACTION_MOVE: {
					finalX = (int) event.getX();
					finalY = (int) event.getY();
					dx = finalX - baseX;
					dy = finalY - baseY;
					if (dy < 0 && dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					} else if (dy > 0 && dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					} else if (dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					}
					if (Math.abs(dx) > Math.abs(dy)) {
						if ((dx > 0 || (dx > 0 && (dy > 0 || dy < 0)))
								&& (startLoc2 < b.getWidth() && startLoc2 > 0)) {
							// Log.e("textSize", "" + b.getWidth());
							flag = 2;
							currentLoc = startLoc2;
							startLoc2 = (int) dis;
							// again();
						}
					}
				}

					return true;
				case MotionEvent.ACTION_UP: {
					// TOUCH COMPLETED
					finalX = (int) event.getX();
					finalY = (int) event.getY();
					dx = finalX - baseX;
					dy = finalY - baseY;
					if (dy < 0 && dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					} else if (dy > 0 && dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					} else if (dx < 0) {
						flag = 1;
						currentLoc = startLoc2;
						startLoc2 = (int) dis;
						// again();
					}
					if (Math.abs(dx) > Math.abs(dy)) {
						if ((dx > 0 || (dx > 0 && (dy > 0 || dy < 0)))
								&& (startLoc2 < b.getWidth() && startLoc2 > 0)) {
							Log.e("textSize", "" + b.getWidth());
							flag = 2;
							currentLoc = startLoc2;
							startLoc2 = (int) dis;
							// again();
						}
					}
				}
					return true;
				}
				return false;
			}
		};
		OnTouchListener disAbleTouch = new OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_UP:
					return true;
				}
				return false;
			}
		};
		RelativeLayout.LayoutParams layoutParams2;

		public Character(Button b, int st, int dis, int speed) {
			this.b = b;
			flag = 0;
			currentLoc = 0;
			this.speed = speed;
			Log.e("Speed", "" + this.speed);
			this.startLoc2 = st;
			this.tim2 = new Timer();
			this.dis = dis;
			r = new Random();
			layoutParams2 = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			layoutParams2.setMargins((int) dis, screenHeight - 150, 0, 0);
			this.b.setLayoutParams(layoutParams2);
		}

		public void createCharacter() {
			this.tim2.scheduleAtFixedRate(new TimerTask() {
				public void run() {
					runOnUiThread(new Runnable() {
						public void run() {
							if (flag == 0) {
								startLoc2 -= 2;
								layoutParams2.setMargins(dis, startLoc2, 0, 0);
								b.setLayoutParams(layoutParams2);
							}
							b.setOnTouchListener(butTouch);
							if (flag == 1)
								movingRightToLeft();
							if (flag == 2)
								movingLeftToRight();
							if (flag == 0) {
								if (startLoc2 + b.getWidth() < 0) {
									turns -= 1;
									if (turns <= 0) {
										destroyCharacters();
										return;
									}
									inActive.add(b);
									again();
								}
							}
						}
					});
				}
			}, 0, speed);
		}

		public void movingRightToLeft() {
			b.setOnTouchListener(disAbleTouch);
			startLoc2 -= 3;
			layoutParams2.setMargins(startLoc2, currentLoc, 0, 0);
			b.setLayoutParams(layoutParams2);
			if (startLoc2 <= imageWidth2)
				if (startLoc > currentLoc) {
					if (startLoc - currentLoc < b.getHeight()) {
						Log.e("startLoc > currentLoc", ""
								+ (startLoc - currentLoc));
						inActive.add(b);
						score += 30;
						if (consectiveThree < 3) {
							consectiveThree += 1;
						}
						tv.setText("Score: " + score);
						// flag = 0;
						again();
					} else if (startLoc2 + b.getWidth() <= 0) {
						inActive.add(b);
						score -= 5;
						if (consectiveThree < 3 && consectiveThree >= 0) {
							consectiveThree -= 1;
						}
						again();
					}
				} else {
					if (currentLoc - startLoc < imageHeight2) {
						Log.e("currentLoc - startLoc", ""
								+ (currentLoc - startLoc));
						inActive.add(b);
						score += 30;
						if (consectiveThree < 3) {
							consectiveThree += 1;
						}
						tv.setText("Score: " + score);
						again();
					} else if (startLoc2 + b.getWidth() <= 0) {
						inActive.add(b);
						score -= 5;
						again();
					}
				}
		}

		public void movingLeftToRight() {
			b.setOnTouchListener(disAbleTouch);
			startLoc2 += 3;
			layoutParams2.setMargins(startLoc2, currentLoc, 0, 0);
			b.setLayoutParams(layoutParams2);
			if (startLoc2 >= screenHeight) {
				inActive.add(b);
				tunnelCounter -= 1;
				progresBar.incrementProgressBy(1);
				if (tunnelCounter <= 0) {
					power += 1;
					progresBar.setProgress(0);
					progresBar2.incrementProgressBy(1);
					tunnelCounter = 10;
				}
				again();
			}
		}

		public void again() {
			if (turns <= 0) {
				destroyCharacters();
				return;
			}
			if (tunnelCounter <= 0) {
				progresBar.setOnTouchListener(progresTouch);
			}

			Button bt;
			b.setOnTouchListener(butTouch);
			flag = 0;
			if (!inActive.isEmpty()) {
				bt = inActive.remove(0);
				startLoc2 = startLoc3;
				dis = (r.nextInt(screenWidth) - 100) + imageWidth;
				layoutParams2.setMargins((int) dis, screenHeight - 150, 0, 0);
				this.b.setLayoutParams(layoutParams2);
				createCharacterAgain();
			}
		}

		public void createCharacterAgain() {
			if (flag == 0) {
				startLoc2 -= 2;
				layoutParams2.setMargins((int) dis, startLoc2, 0, 0);
				b.setLayoutParams(layoutParams2);
			}
			b.setOnTouchListener(butTouch);
			if (flag == 1)
				movingRightToLeft();
			if (flag == 2)
				movingLeftToRight();
			if (flag == 0) {
				if (startLoc2 + b.getWidth() < 0) {
					turns -= 1;
					if (turns <= 0) {
						destroyCharacters();
						return;
					}
					inActive.add(b);
					again();
				}
			}
		}
	}
}