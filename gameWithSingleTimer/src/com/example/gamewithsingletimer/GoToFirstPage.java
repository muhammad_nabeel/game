package com.example.gamewithsingletimer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class GoToFirstPage extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Intent intent = new Intent( GoToFirstPage.this , GameMenu.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

}
